(defproject todo "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [ring/ring-defaults "0.3.2"]
                 [compojure "1.6.1"]]
  :ring {:handler todo.core/app :nrepl {:start? true}}
  :plugins [[swvist/lein-ring "0.12.5+nrepl0.6.0"]])
