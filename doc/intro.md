```clojure
(defn fizzbuzz [n]
  (letfn [(fizz? [n] (zero? (mod n 3)))
          (buzz? [n] (zero? (mod n 5)))
          (fizzbuzz? [n] ((every-pred fizz? buzz?) n))]
    (cond
      (fizzbuzz? n) "fizzbuzz"
      (fizz? n) "fizz"
      (buzz? n) "buzz"
      :else n)))
```

```clojure
[compojure.core :refer [defroutes GET POST]]
[compojure.route :refer [resources]]
[ring.util.response :refer [redirect]]
[ring.middleware.defaults :refer [wrap-defaults api-defaults]]
[hiccup.page :refer [html5 include-css]]
[hiccup.form :refer [form-to text-field submit-button]]
```

#### Primitives
* Strings
* Regex
* Characters
* Numbers
  * Integer and BigInt
  * Floating point and BigDecimal
  * Rational
* Symbols

#### Collections
* Lists
* Vectors
* Maps
* Keywords
* Sets

#### Special forms
* `def`
* `fn`
* `let`
* `if`
* `loop` and `recur`
* `.` and `new`

#### Functions
* `defn`
* Docstrings
* Multi-arity
* Variadic
* Destructuring

#### Homoiconicity
* Quoting
* Lists again
* REPL
* Macros